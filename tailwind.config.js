/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
        extend: {
            spacing: {
                '13': '3.1875rem',
                '23': '5.75rem',
            },
            colors: {
                'slate-950': '#000116',
                'slate-900': '#010B40'
            },
            fontSize: {
                '4xl': '2.5625rem'
            },
            backgroundImage: {
                'gradient-radial-bottom': 'radial-gradient(ellipse at bottom, var(--tw-gradient-stops))',
            },
            scale: {
                '200': '2'
            }
        },
    },
    plugins: [],
}


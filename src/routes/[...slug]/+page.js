import pages from '$lib/data/pages.js'
import { error } from '@sveltejs/kit';

export function entries() {
	return pages;
}

export function load({params}) {

	const page = pages.find(page => params.slug === page.slug);
    
    if (!page) throw error(404);

	const title = page.title;

	return {title};
}
export default [
    {
        title: 'Animation',
        slug: 'animation'
    },
    {
        title: 'Branding',
        slug: 'branding'
    },
    {
        title: 'Illustration',
        slug: 'illustration'
    }
];